#!/usr/bin/env bash

# Copied from https://gist.github.com/darrenpmeyer/fa427b1ebdd8239bf665

set -e

oc_ver="8.09"

# Deve ser root
[ ${UID} -ne 0 ] && echo "You must be root to run this script." && exit 1

## Preflight
sudo apt-get install curl vpnc-scripts build-essential gettext pkg-config libssl-dev libxml2-dev liblz4-dev libstoken1 stoken
curl -O ftp://ftp.infradead.org/pub/openconnect/openconnect-${oc_ver}.tar.gz
curl -O ftp://ftp.infradead.org/pub/openconnect/openconnect-${oc_ver}.tar.gz.asc
gpg --keyserver pgp.mit.edu --recv-key 67e2f359

if gpg --verify openconnect-${oc_ver}.tar.gz.asc 2>/dev/null; then
  echo -e "\n++++ GPG Signature Verified OK! ++++\n\n"
else
  gpg --verify openconnect-${oc_ver}.tar.gz.asc # to show output
  echo -e "\n!!!! GPG Signature FAILED. Not proceeding with autobuild !!!!\n\n"
  exit 127
fi

tar xzf openconnect-${oc_ver}.tar.gz
cd openconnect-${oc_ver} || exit

if ! (./configure --without-gnutls --with-vpnc-script=/usr/share/vpnc-scripts/vpnc-script); then
  echo "!! Configuration was not successful, not proceeding with autobuild"
  exit 1
fi

if ! (make); then
  echo "!! build was not successful, not proceeding with install"
  exit 2
fi

if ! (sudo make install); then
  echo "!! installation failed"
  exit 3
fi

if ! (sudo ldconfig /usr/local/lib); then
  echo "?? error running ldconfig; this MAY be a problem"
  echo "   but we will not exit with an error for it"
fi

exit 0
