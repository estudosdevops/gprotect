# CHANGELOG

<!--- next entry here -->

## 1.0.0
2020-05-22

### Features

- **added:** initial commit (c409dd6ece6fa12de9c76c752a6c545411d4f258)
- **added:** new function (80dc72b963f41168e59361447a0bae4bf7854bdd)
- **added:** new functiona (49e126a7423e85c87058a93861b6d262954421c1)
- **added:** new functions (ff008ea5fcc45771f31f32e5ff61ee3e17aecfd5)
- version major (935760d5c9495336c3f0ce2e341307dbdb3630c7)

### Fixes

- **rename:** Text change to English (55f45ed3bd1d18c20b32d00c24bfc81a50763eb4)
- **update:** Install (a42eeb7bc26ac31accc1ab7f7239cfb307f3156c)
- **ci:** Test CI (953e3dcc1209bbd5466f65e7d144f9361f48a061)

## 0.1.0
2020-05-22

### Features

- **added:** initial commit (c409dd6ece6fa12de9c76c752a6c545411d4f258)
- **added:** new function (80dc72b963f41168e59361447a0bae4bf7854bdd)
- **added:** new functiona (49e126a7423e85c87058a93861b6d262954421c1)
- **added:** new functions (ff008ea5fcc45771f31f32e5ff61ee3e17aecfd5)

### Fixes

- **rename:** Text change to English (55f45ed3bd1d18c20b32d00c24bfc81a50763eb4)
- **update:** Install (a42eeb7bc26ac31accc1ab7f7239cfb307f3156c)
- **ci:** Test CI (953e3dcc1209bbd5466f65e7d144f9361f48a061)