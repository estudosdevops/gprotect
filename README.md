<h1 align="center">Welcome to gprotect 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.1.0-blue.svg?cacheSeconds=2592000" />
  <a href="https://gitlab.com/estudosdevops/gprotect/-/blob/master/README.md" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="https://www.gnu.org/licenses/licenses.pt-br.html" target="_blank">
    <img alt="License: GNU" src="https://img.shields.io/badge/License-GNU-yellow.svg" />
  </a>
  <a href="https://twitter.com/fcruzcoelho" target="_blank">
    <img alt="Twitter: fcruzcoelho" src="https://img.shields.io/twitter/follow/fcruzcoelho.svg?style=social" />
  </a>
</p>

> It uses the Palo Alto Networks (PAN) openconnect VPN GlobalProtect SSL Compatible project

## Motivation

Development of gprotect was started after a trial of the Palo Alto client under Linux found it to have many deficiencies when the certificate was invalid

## Dependencies

- Openconnect

```sh
sudo ./oc_install.sh
```

- gprotect.conf

```sh
cat <<EOF >$HOME/.config/gprotect.conf
protocol = gp
user = fulano.silva # Change to your username
pid-file = /var/run/openconnect.pid
servercert = pin-sha256:tshIkwa9zrqyIwxzcH+asasasas= # Change to your HASH
background
syslog
EOF
```

## Install

```sh
curl -L https://gitlab.com/estudosdevops/gprotect/-/raw/master/gprotect -o /usr/local/bin/gprotect && chmod +x /usr/local/bin/gprotect
```

## Usage

Adding the following config to .bashrc/.zshrc

```sh
export PORTAL="URL or IP VPN"
```

```sh
sudo gprotect [ connect | disconnect | reconncet | status]
```

## Author

👤 **Fabio Coelho**

* Twitter: [@fcruzcoelho](https://twitter.com/fcruzcoelho)
* GitLab: [@fabiocruzcoelho](https://gitlab.com/fabiocruzcoelho)
* LinkedIn: [@fcruzcoelho](https://linkedin.com/in/fcruzcoelho)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/estudosdevops/gprotect/-/issues). 

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2020 [Fabio Coelho](https://github.com/fabiocruzcoelho).<br />
This project is [GNU](https://www.gnu.org/licenses/licenses.pt-br.html) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_